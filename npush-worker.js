/**
 *	NEWICON NPUSH SERVER
 * 
 *	@author	Krzysztof Stasiak <chris.stasiak@newicon.net>
 *	@name	npush-worker.js
 * 
 *	Copyright(c) 2013 NEWICON LTD <newicon@newicon.net>
 *	GPL Licensed
 *	
 */

var npes = require('./engines/socket'),
	npep = require('./engines/post'),
	npp  = require('./protocols/npush-protocol'),
	nps  = require('./stats/npush-stats'),
	os   = require('os-utils');	

/**
 * Create worker instance
 * 
 * @param int    port
 * @param object options
 * @returns {createWorker.worker}
 */
exports.createWorker = function(port, options) {	
	// this object keeps all data about worker process
	var worker = {
		'app' : null,						// http server
		'io' : null,						// socket.io server
		'db' : null,						// mongoDB connection
		'system' : detectOS(),
		'workersInstances' : new Array(),
		'nicknames' : {},
		'activity' : {},
		'activityGlobal' : new Object(),
		'nicknameGlobal' : new Object(),
		'activeSocketsByWorkers' : new Object(),
		'allowGlobalTraffic' : options.allowGlobalTraffic,
	}
	// master process message listener
	worker.listenMaster = function() {
		addMasterListener(worker, options);
		// return this for chain callbacks
		return this;
	}
	// create basis
	var db = worker.db = createMongoDBConnection(options);
	var app = worker.app = createHTTPServer(port, options);
	var io = worker.io = createSocketIOServer(app, db, worker, options);	
	// engine and rules
	initNPushEngines(app, io, db, worker, options);
	// return worker instance; for chain callbacks
	return worker;
}

/**
 * Allow worker to listen the master process
 * 
 * @param {object} worker
 * @param {object} options
 */
function addMasterListener(worker, options) {
	process.on('message', function(data) {
		npp.ModulePusherMessageHandler(data, worker, options);
	});
}

/**
 * Open connection to mongo
 * 
 * @param {object} options
 * @returns {mongoDBInstance}
 */
function createMongoDBConnection(options) {
	return require("mongojs").connect(options.MongoDB.url, options.MongoDB.collections);
}

/**
 * Create HTTP Server
 * 
 * @param int port
 * @returns {express}
 */
function createHTTPServer(port) {

	var express = require('express'),
		app = express.createServer(),
		stylus = require('stylus'),
		qs = require('querystring');

	app.configure(function () {
		app.use(stylus.middleware({
			src: __dirname + '/public', 
		}));
		app.use(express.static(__dirname + '/public'));
		app.set('views', __dirname);
		app.set('view engine', 'jade');
	});
	app.use(express.bodyParser());
	app.get('/', function (req, res) {
		res.render('public/index', {
			layout: false
		});
	});
	/**
	 * Test POST e.g:
	 * $ telnet localhost 8090
	 * POST / HTTP/1.0[enter]
	 * content-length:10[enter][enter]
	 * value=1234[enter]
	 */
	app.post('/', function(req, res) {
		var body = '';
		req.on('data', function(data) {
			body += data;
		});
		req.on('end', function () {
			try {
				res.send('DATA:'+JSON.stringify(qs.parse(body))+'\n');
			} catch(e) {
				res.send('ERROR');
			}
		});
	});
	app.listen(port, function () {
		var addr = app.address();
		console.log(' Worker PID ' + process.pid + ' is listening on http://' + addr.address + ':' + addr.port);
	});		
	return app;
}

/**
 * Create Socket.io instance
 * 
 * @param object app
 * @param int    pid
 * @returns {socket.io}
 */
function createSocketIOServer(app, db, worker, options) {
	var sio = require('socket.io'),
		io = sio.listen(app);

	io.configure('production', function(){
		io.enable('browser client minification');  // send minified client
		io.enable('browser client etag');          // apply etag caching logic based on version number
		io.enable('browser client gzip');          // gzip the file
		io.set('log level', 1);                    // reduce logging
		io.set('transports', [                     // enable all transports (optional if you want flashsocket)
			'websocket'
		, 'flashsocket'
		, 'htmlfile'
		, 'xhr-polling'
		, 'jsonp-polling'
		]);
		io.set('store', new sio.RedisStore(options.RedisServer));	
	});
	io.configure('development', function(){
		io.set('transports', ['websocket']);
		io.set('store', new sio.RedisStore(options.RedisServer));				
	});
	/**
	*	Socket.io configuration for:
	*	
	*	Authorisation process. This function looking for API key and on success
	*	return object with proper socket data.
	*	
	*	@param object handshakeData
	*	@param function callback
	*/
	io.configure(function (){
		io.set('authorization', function (handshakeData, callback) {
			if(!options.allowGlobalTraffic && options.NPUSH_SERVER_STATUS_APIKEY != handshakeData.query.k
					&& options.NPUSH_SERVER_CONTROL_APIKEY != handshakeData.query.k) {
						return;
					}
			db.collection('apis')
			.find(JSON.parse('{ "key" : "' + handshakeData.query.k + '", "enabled" : "true" }'))
			.forEach(function(err, doc) {
				if(!doc && options.NPUSH_SERVER_STATUS_APIKEY != handshakeData.query.k
						&& options.NPUSH_SERVER_CONTROL_APIKEY != handshakeData.query.k) {
					callback(null, false);
				} else {
					handshakeData.client_room = handshakeData.query.k;
					handshakeData.client_wellcome_msg = 'Welcome in Newicon Pusher System V.2';
					handshakeData.address = handshakeData.address.address;
					handshakeData.port = handshakeData.address.port;
					handshakeData.api_key = handshakeData.query.k;
					handshakeData.channel = handshakeData.query.c;
					handshakeData.user_id = handshakeData.query.id;
					handshakeData.user_name = handshakeData.query.name;
					handshakeData.user_room = handshakeData.query.room;
					nps.PusherAPIsActivity(worker, {client:handshakeData.query.k}, 0, 1, 0, 0);
					callback(null, true);
				}
			});		
		});
	});		
	return io;
}

/**
 * Init NPush Engine
 * 
 * @param {express} app
 * @param {socketio} io
 */
function initNPushEngines(app, io, db, worker, options) {
	// add npep => NPush HTTP/POST Engine
	npep.createNPushHttpPostEngine(app, io, db, worker);
	// add npes => NPush Socket.io Engine
	npes.createNPushSocketIOEngine(io, db, worker);		
}

/**
 * Detect Operating System
 * 
 * @param object worker
 */
function detectOS() {
	return os.platform() == 'linux' ? 1 : 2;
}