/**
 *	NEWICON NPUSH SERVER
 * 
 *	@author	Krzysztof Stasiak <chris.stasiak@newicon.net>
 *	@name	npush-master.js
 * 
 *	Copyright(c) 2013 NEWICON LTD <newicon@newicon.net>
 *	GPL Licensed
 *
 *	Master Server Instance
 *	1. Master instance forks workers up to startInstancesNumber number.
 *	2. Master process receive worker messages and send own to workers.
 *	3. Master process manage load-balancer of TCP traffic in cluster.
 */

var npp = require('./protocols/npush-protocol'),
	cluster = require('cluster');

/**
 * Create master instance
 * 
 * @param object options
 */
exports.createMaster = function(options) {
	// this object should keep all statistics sockets, db, process data for
	// all running instaneces
	var master = {
		w:new Array(),				    // worker PID list
		a:new Object(),					// activity for all socket
		n:new Object(),					// global nicknames
		i:new Object(),					// instances by PID with 
										// number of active sockets
		t:{								// traffic
			a:true						// allow traffic
		},
		'statsTimer' : null,			// stats setTimeout handler
	};
	var buffer = {
		'activityGlobal' : new Array(),	// store data received from master
		'nicknameGlobal' : new Array()	// not in used <=3.0 version
	}
	master.listenWorkers = function() {
		addClusterListener(master, buffer);
		// return this for chain callbacks
		return this;
	}
	master.broadcastStats = function() {
		master.statsTimer = setTimeout(function(master, _buffer){
			broadcastStatistics(master, _buffer);
		}, 1000, master, buffer);	
		// return this for chain callbacks
		return this;
	}
	// create workers processes
	for(var i = 0; i < options.startInstancesNumber; i++) {
		cluster.fork();		
	}
	// store IDs of created workers
	Object.keys(cluster.workers).forEach(function(id) {
		master.w.push(cluster.workers[id].process.pid);
	});
	// do this when worker died
	cluster.on('exit', function(worker, code, signal) {
		console.log('worker ' + worker.process.pid + ' died');
	});	
	// return this for chain callbacks
	return master;
}

/**
 * Allow master to listen the workers processes
 * 
 * @param {object} master
 * @param {object} buffer
 */
function addClusterListener(master, buffer) {
	Object.keys(cluster.workers).forEach(function(id) {
		cluster.workers[id].on('message', function(data) {
			messageHandler(this.id, data, master, buffer);
		});
	});	
}

/**
 * Action when worker send a message to master
 * FROM WORKER TO MASTER
 * 
 * @param int    workerId
 * @param object data
 * @param object master
 * @param object buffer
 */
function messageHandler(workerId, data, master, buffer) {
	if(data.event == 'worker-activity') {
		// get feedback from worker if master send 'send-activity' event
		for(s in data.data.a) {
			if(!buffer.activityGlobal[s]) {
				buffer.activityGlobal[s] = new Array();
			}
			buffer.activityGlobal[s][workerId] = data.data.a[s];			
		}
		// merge nickanmes for all workers
		for(a in data.data.n) {
			for(c in data.data.n[a]) {
				for(r in data.data.n[a][c]) {
					for(u in data.data.n[a][c][r]) {
						if(!master.n[a]) master.n[a] = new Object();
						if(!master.n[a][c]) master.n[a][c] = new Object();
						if(!master.n[a][c][r]) master.n[a][c][r] = new Object();
						master.n[a][c][r][u] = data.data.n[a][c][r][u];
					}
				}
			}
		}
	} else if(data.event == 'system-command') {
		if(data.data.action) {
			if(data.data.action == 'traffic') {
				if(data.data.value == '0') {
					master.t.a = false;
				} else if(data.data.value == '1') {
					master.t.a = true;
				}
			}
		}
	}
}
	
/**
 * Calculate stats for all workers
 * 
 * @param {object} master
 * @param {object} buffer
 */
function broadcastStatistics(master, buffer) {	
	for(s in buffer.activityGlobal) {				
		for(w in buffer.activityGlobal[s]) {
			if(!master.a[s]) {
				master.a[s] = npp.PusherAPIStatsRecordCreate();
			}
			master.a[s].s.m += buffer.activityGlobal[s][w].s.m;
			master.a[s].s.pm += buffer.activityGlobal[s][w].s.pm;
			master.a[s].s.dbrps += buffer.activityGlobal[s][w].s.dbrps;
			master.a[s].s.dbwps += buffer.activityGlobal[s][w].s.dbwps;
			master.a[s].s.dbups += buffer.activityGlobal[s][w].s.dbups;					
			master.a[s].n.as += buffer.activityGlobal[s][w].n.as;
			if(!master.i[buffer.activityGlobal[s][w].i.pid]) {
				master.i[buffer.activityGlobal[s][w].i.pid] = 0;
			}
			master.i[buffer.activityGlobal[s][w].i.pid] += buffer.activityGlobal[s][w].n.as;
			if(buffer.activityGlobal[s][w].c.s) {
				master.a[s].c.s = true;
			}
		}
	}	
	// broadcast data to workers
	// FROM MASTER TO WORKERS
	Object.keys(cluster.workers).forEach(function(id) {
		// send data to worker who send message to master
		cluster.workers[id].send({event:'global-stats', data:master}); 
		// ask for current stats from worker
		cluster.workers[id].send({event:'send-activity'});		
	});
	// reset status before ask workers for new one
	master.a = new Object();
	master.n = new Object();
	master.i = new Object();
	buffer.activityGlobal = new Array();
	// set new timeout
	master.statsTimer = setTimeout(function(master, buffer){
		broadcastStatistics(master, buffer);
	}, 1000, master, buffer);
}