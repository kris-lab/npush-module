<?php
/*
 * BASIC STRESS APPLICATION
 */
?>

<script src="../lib/npush.js" type="text/javascript" rel="stylesheet" /></script>
<link rel="stylesheet" type="text/css" href="styles/chat.css">

<div class="container" id="chat">
	<div id="messages" style="background-color: white; height:auto; overflow:auto;">
        <div class="row-fluid" style="padding-bottom: 5px; padding-top: 5px;">
            <div class="span4"></div>
            <div class="span3">		
                <input id="stressSocketsNumber" onChange="this.value = parseInt(this.value);" style="width:90%;border-radius:7px;border:1px solid #eee;padding:3px;margin:2px;text-align:center;" placeholder=" e.g. 1" value="1">
            </div>
            <div class="span5" style="padding-top:2px;">
                <div class="btn-group">
                    <a href="#" id="stressStartWatching" class="btn btn-danger"><i class="icon-play"></i></a>
                    <a href="#" id="stressStopWatching" class="btn btn-info"><i class="icon-stop"></i></a>
                </div>
            </div>
        </div>
        <div class="row-fluid"><br></div>
        <div class="row-fluid" style="text-align: center;"><span class="badge badge-success" id="stressStatus">Stress is working now</span></div>
        <div class="row-fluid"><br></div>
        <div class="row-fluid">
            <div class="span1"></div>
            <div class="span10">
                <div class="alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;">
                    <strong>Stress Status</strong>
                    <div class="row-fluid" id="stressData">  
                        waiting for data...
                    </div>
                </div>
            </div>
            <div class="span1"></div>
        </div>           
        <div id="connecting">
                <div class="wrap module-loading-spinner">Connecting to Push Notification Server</div>
        </div>        
        <div id="nicknames" style="display:none;"></div>			
        <div id="userschannel" style="display:none;"></div>
        <div id="usersapplist" class="hide alert alert-info" style="border: 1px #ddd solid; color:#666; background-color: #eee; margin-bottom:6px; margin-left:10px; margin-right: 5px;"></div>
        <div id="channels" class="hide alert alert-success" style="margin-bottom:6px; margin-left:10px; margin-right: 5px;"></div>			
        <div id="lines" style="display:none;"></div>
	</div>
	<div id="history" style="display:none;"></div>
</div>

<script>
	
	var mychannel = null;
    var stressStatus = 0;
	
    var socketsDefualtConnectionNumber = 1;
    var testStreeChannel = new Array();
    
    var currentChannelName = '<?php echo $channel; ?>';
    var currentRoomName = '<?php echo $room; ?>';
    var currentDataPattern = {};
    
    function getStressSocketsStatus() {		
		
        var _connected = 0;		
		var _mintime = -1;
		var _maxtime = 0;
		var _msgmintime = -1;
		var _msgmaxtime = 0;
        var _html = '<table class="table"><thead><th>Connected in</th><th>Status</th><th>Recevied data</th><th>Delivery time</th></thead><tbody>';
        
        for(socket in testStreeChannel) {
            if(testStreeChannel[socket][4] != undefined) {
                
                var _time = '';                
                if(testStreeChannel[socket][6] != 0) {
                    _time = new Date(testStreeChannel[socket][6]);
                    _time = _time.getHours() + ':' + _time.getMinutes() + ':' + _time.getSeconds() + '.' + _time.getUTCMilliseconds();
                }
				
				var _respond_time = testStreeChannel[socket][2]-testStreeChannel[socket][1];
				if(_respond_time < _mintime || _mintime == -1)
					_mintime = _respond_time;
				
				if(_respond_time > _maxtime)
					_maxtime = _respond_time;
                
				if(testStreeChannel[socket][6] < _msgmintime || _msgmintime == -1)
					_msgmintime = testStreeChannel[socket][6]
				
				if(testStreeChannel[socket][6] > _msgmaxtime)
					_msgmaxtime = testStreeChannel[socket][6];
				
                _html = _html + '<tr><td>' + (_respond_time)/1000 
                                                    + ' s</td><td>' + testStreeChannel[socket][4] 
                                                    + '</td><td>' + testStreeChannel[socket][5] 
                                                    + '</td><td>' + _time;
                                                    + '</td></tr>';
                            
               if(testStreeChannel[socket][4] == 'connected') _connected++;
            }            
        }
		if((_connected/socketsDefualtConnectionNumber)*100 % 10 == 0) {
			$('#stressData').empty().append(_html + '</tbody></table>');
			$('#stressStatus').addClass('badge-success').html('Stress is active - connected ' + _connected + ' (min. delay is ' + _mintime/1000 + 's, max. delay is ' + _maxtime/1000 + 's, msg. delivery diff: '+ (_msgmaxtime-_msgmintime)/1000 +'s)');        
			$('#chat').addClass('connected');
			$('#chat').addClass('nickname-set');         
		}
    }
    
    function ModulePusherSubscribeServiceChannel() {
		npush.init({ 
				'apiKey' : '<?php echo $apiId; ?>',
				'url' : '<?php echo $server['push_address']; ?>', 
				'port' : <?php echo $server['push_port']; ?>, 
				'user_id' : '<?php echo $userId; ?>', 
				'user_name' : '<?php echo $userName; ?>'
			}		
		);		 
        for(x = 0; x < socketsDefualtConnectionNumber; x++) {
            (function(i) {
                testStreeChannel[i] = new Array();

                testStreeChannel[i][1] = (new Date()).getTime();
                testStreeChannel[i][5] = '';
                testStreeChannel[i][6] = 0;
                testStreeChannel[i][0] = npush.subscribe(currentChannelName+'-'+i+'/'+currentRoomName);
                testStreeChannel[i][0].on('message', function(dataUser, dataSystem) {
                    testStreeChannel[i][5] = dataUser;                    
                    testStreeChannel[i][6] = (new Date()).getTime();    
                    getStressSocketsStatus();
                });

               testStreeChannel[i][0].on('connect', function() {
                    testStreeChannel[i][2] = (new Date()).getTime();
                    testStreeChannel[i][4] = 'connected';
                    testStreeChannel[i][5] = '';
                    testStreeChannel[i][6] = 0;					
                    getStressSocketsStatus();
                });     

               testStreeChannel[i][0].on('disconnect', function() {
                    testStreeChannel[i][3] = (new Date()).getTime();
                    testStreeChannel[i][4] = 'disconnected';
                    getStressSocketsStatus();
                });        
                
               testStreeChannel[i][0].on('connecting', function() {
                    testStreeChannel[i][1] = (new Date()).getTime();
                    testStreeChannel[i][4] = 'connecting';
                    getStressSocketsStatus();
                }); 
            })(x);
		}
    }    

    // dom manipulation
    $(document).ready(function() {
		viewUpdate();
        socketsDefualtConnectionNumber = $('#stressSocketsNumber').val();
		ModulePusherSubscribeServiceChannel();
		$(window).resize(function() {
			viewUpdate();
		});
        $('#stressStartWatching').click(function() {
           $('#stressStatus').addClass('badge-success').html('Stress is working now...');        
           for(channel in NIPUSH.channels) {
               
                NIPUSH.channels[channel].disconnect();           
           }
           testStreeChannel = new Array();
           getStressSocketsStatus();
           try {               
                socketsDefualtConnectionNumber = $('#stressSocketsNumber').val();
                // connect to new channel
                ModulePusherSubscribeServiceChannel();
           } catch(e) {
               alert('Wrong data object: ' + e.message);
           }
        });
        $('#stressStopWatching').click(function() {                    
            for(channel in NIPUSH.channels) {
                  NIPUSH.channels[channel].disconnect();    
            }
            testStreeChannel = new Array();
            getStressSocketsStatus();
            $('#stressStatus').removeClass('badge-success').html('Stress is stopped now');            
        });
    });
	
    // refreshing view on resize
	function viewUpdate() {
		$('#messages').height($(window).height()-110);
	}
</script>