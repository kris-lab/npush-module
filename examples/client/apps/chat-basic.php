<?php
/*
 * BASIC CHAT APPLICATION
 */
?>

<script src="../lib/npush.js" type="text/javascript" rel="stylesheet" /></script>
<link rel="stylesheet" type="text/css" href="styles/chat.css">

<div id="chat">
	<div id="nickname">
			<form id="set-nickname" class="wrap">
					<p>Please type in your nickname and press enter.</p><input id="nick"><p id="nickname-err">Nickname already in use</p>
			</form>
			<div>
				<select id="room">
						<option>room</option>
				</select>
				<select id="channel">
						<option>channel</option>
				</select>
			</div>
	</div>
	<div id="connecting">
			<div class="wrap module-loading-spinner">Connecting to Push Notification Server</div>
	</div>
	<div id="messages" style="background-color: white; height:auto;">
			<div id="nicknames"></div>			
			<div id="userschannel"></div>
			<div id="usersapp" style="display:none;"></div>
			<div id="lines"></div>
	</div>
	<div id="history"></div>
</div>

<script>
	var mychannel = null;
	
    function ModulePusherSubscribeServiceChannel() {

		npush.init({ 
				'apiKey' : '<?php echo $apiId; ?>',
				'url' : '<?php echo $server['push_address']; ?>', 
				'port' : <?php echo $server['push_port']; ?>, 
				'user_id' : '<?php echo $userId; ?>', 
				'user_name' : '<?php echo $userName; ?>'
			}		
		);		
				
		mychannel = npush.subscribe('<?php echo $channel; ?>/<?php echo $room; ?>', {
				'connect' : ModulePusherSubscribeService_connected,
				'connecting' : ModulePusherSubscribeService_connecting,
				'connect_fail' : null,
				'reconnect' : ModulePusherSubscribeService_reconnect,
				'reconnecting' : ModulePusherSubscribeService_reconnecting,
				'reconnect_fail' : null,
				'disconnect' : ModulePusherSubscribeService_disconnect,
				'error' : ModulePusherSubscribeService_error,
				'message' : ModulePusherSubscribeService_messageNative,
				'announcement' : ModulePusherSubscribeService_announcement,
				'usersroom' : ModulePusherSubscribeService_nicknames,
				'usersapp' : ModulePusherSubscribeService_nicknamesapp,
				'userschannel' : ModulePusherSubscribeService_nicknameschannel
			} 		
		);
		
		mychannel.on('message', function(dataUser, dataSystem) {
			ModulePusherSubscribeService_message({'dataUser':dataUser, 'dataSystem' : dataSystem });			
		});	
		
		/*
		 *	BASIC EXAMPLE
		 
			mychannel = npush.subscribe('mychannel');
			
			mychannel.on('message', function(message) { 

				alert(message); 
			});			
	
			mychannel.trigger('message', 'Hello world!);
		 
		 *	ADVANCED EXAMPLE:

			// init push

			npush.init({
						'apiKey' : 'your_api_key',
						'url' : 'push_server_ip_or_url', 
						'port' : 'push_server_port', 
						'user_id' : 'id_as_number_or_string',	// allow use history
																// as user activity
						'user_name' : 'name_asnumber_or_String'
					});
	
			// create channel
		
			mychannel = npush.subscribe('mychannel');
			mychannel = npush.subscribe('mychannel/myroom');
			mychannel = npush.subscribe('mychannel/myrooms/family');
			mychannel = npush.subscribe('mychannel', { 
														connect : function() {},
														disconnect : function() {},
														error : function() {}
													});
	
			// events
			// event_name is any string
	
			mychannel.on('event_name', function(dataUser) { 

				alert(dataUser); 
			});	
	
			mychannel.on('event_name', function(dataUser, dataSystem) { 

				alert(dataUser + ' ' + dataSystem); 
			});

			// user activity
			// to work with activity you have to provide uniqe USER_ID on init
	
			mychannel.activity(0);		// from last activity on channel/room
			mychannel.activity(60000);	// from last 60 seconds
	
			// send message
	
			mychannel.trigger('event_name', _msg);		// send to api/channel/room/*
			mychannel.channel('event_name', _msg);		// send to api/channel/*
			mychannel.app('event_name', _msg);			// send to api/*
	
			// system events, you can add callback on channel subscribe
				- connect			callback()
				- connecting		callback()
				- connect_fail		callback()
				- reconnect			callback()
				- reconnecting		callback()
				- reconnect_fail	callback()
				- disconnect		callback()
				- error				callback(e)
				- message			callback(dataObject) 
									-> dataObject = { 
												dataUser: StringOrObject, 
												dataSystem: {
													
													client		: String
													room		: String
													channel		: String
													time		: Int
													user_id		: String
													user_name	: String
													type		: Int
													archive		: Int
												}, 
												event : String 
											}
				- announcement		callback(dataString)
				- usersroom			callback(dataArray)
				- userschannel		callback(dataArray)
				- usersapp			callback(dataArray)
		 */
    }    
	
    function ModulePusherSubscribeService_connected() {
        $('#chat').addClass('connected');
        $('#chat').addClass('nickname-set'); 
        // get activity since last USERID diconection
        mychannel.activity();
    }

    function ModulePusherSubscribeService_connecting() {
        // here goes implementation
    };

    function ModulePusherSubscribeService_announcement(msg) {
		$('#lines').append($('<p>').append($('<em>').text(msg)));
    }

    function ModulePusherSubscribeService_nicknames(nicknames) {
		$('#nicknames').empty().append($('<span>Online in my room: </span>'));
		for (var i in nicknames) {
		  $('#nicknames').append($('<b>').text(nicknames[i]));
		}
    }

    var _max_connection = 0;
    function ModulePusherSubscribeService_nicknamesapp(app) {
		var _string = '';
		var _count = 0;
		for (var a in app) {
			for (var r in app[a]) {
				for (var u in app[a][r]) {
					_string = _string + '<b>' + app[a][r][u] + '</b>';
						_count++;
				}
			}
		}
		if(_count > _max_connection) _max_connection = _count;
		var _time = new Date();
		$('#usersapp').empty().append($('<span>Online app (' + _count + ', max: ' + _max_connection + '): </span>'));
		$('#usersapp').append(_string);
    }

    function ModulePusherSubscribeService_nicknameschannel(rooms) {
		$('#userschannel').empty().append($('<span>Online in rooms on my channel: </span>'));
		for (var r in rooms) {
			  for (var u in rooms[r]) {
				  $('#userschannel').append($('<b>').text(rooms[r][u]));
			  }
		}
    }

    function ModulePusherSubscribeService_reconnect() {
		ModulePusherSubscribeService_message('System', {msg: { object : 'Reconnected to the server', data : '' } } );
    }

    function ModulePusherSubscribeService_disconnect() { 
		try {
			ModulePusherSubscribeService_message('System', {msg: { object : 'disconnected', data : '' } } );
		} catch(e) { }
    } 

    function ModulePusherSubscribeService_reconnecting() {
		ModulePusherSubscribeService_message('System', {msg: { object : 'Attempting to re-connect to the server', data : '' } } );
    }

    function ModulePusherSubscribeService_error(e) {
		try {
			ModulePusherSubscribeService_message('System', e ? e : {msg: { object : 'A unknown error occurred', data : '' } } );
		} catch(e) { }			
    }

	function ModulePusherSubscribeService_messageNative(data) {
		// here goes implementation
	}

    function ModulePusherSubscribeService_message(msg) {
        var _time = new Date(msg.dataSystem.time);
        var _time_string = _time.getHours() + ":" + _time.getMinutes();
		try {
			var _data = "JSON data correct : " + JSON.stringify(JSON.parse(msg.dataUser));
		} catch(e) {
			
			if(msg.dataUser != undefined && msg.dataUser != '') {
				var _data = 'Wrong JSON data : ' + msg.dataUser;
			} else {
				var _data = '';
			}
		}
		var _msg = msg.dataUser;
        $('#lines').append($('<p>').append($('<b>').text(msg.dataSystem.user_name + ' ' + msg.dataSystem.room + ' ' + msg.dataSystem.channel + ' ' + _time_string + ' type:' + msg.dataSystem.type + ' archive:' + msg.dataSystem.archive + ' source: ' + msg.dataSystem.source + ' : '), '<br>' + _msg + ' ; ' + _data));
    }
    
    // dom manipulation
    $(document).ready(function() {
		viewUpdate();
		ModulePusherSubscribeServiceChannel();
		$(window).resize(function() {
			viewUpdate();
		});
    });
    
    // Send functions called from parent windows
    // this chat app is used in iframe window
	function sendMessage(_msg) {
		mychannel.trigger('message', _msg);
        $('#lines').get(0).scrollTop = 10000000;
	}
	
	function sendMessageToChannel(_msg) {
		mychannel.channel('message', _msg);		
        $('#lines').get(0).scrollTop = 10000000;
	}	
	
	function sendMessageToApp(_msg) {
		mychannel.app('message', _msg);		
        $('#lines').get(0).scrollTop = 10000000;
	}       
    
    // refreshing view on resize
	function viewUpdate() {
		$('#lines').height($(window).height()-155);
	}
</script>